package mk.test.cardwar;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

public class ScoreActivity extends AppCompatActivity {

    TextView playerScore, computerScore, tiesScore;
    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_score);

        ActionBar actionBar = getSupportActionBar();
        //actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#312E2E")));
        actionBar.setDisplayHomeAsUpEnabled(true);

        sharedPreferences = getSharedPreferences("MY_SHARED_PREF", MODE_PRIVATE);

        playerScore = findViewById(R.id.playerScore);
        computerScore = findViewById(R.id.computerScore);
        tiesScore = findViewById(R.id.ties);

        playerScore.setText("Player wins:   " + sharedPreferences.getInt("playerScore",0));
        computerScore.setText("Computer wins:   " + sharedPreferences.getInt("computerScore",0));
        tiesScore.setText("Ties:   " + sharedPreferences.getInt("ties",0));
    }

    public void onClear(View view) {
        playerScore.setText("Player wins:   " + 0);
        computerScore.setText("Computer wins:   " + 0);
        tiesScore.setText("Ties:   " + 0);
        Toast.makeText(this, "Score reset successfully",Toast.LENGTH_SHORT).show();

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.commit();
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        finish();
        return super.onOptionsItemSelected(item);
    }
}
