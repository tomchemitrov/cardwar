package mk.test.cardwar;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    TextView playerTop, playerCenter, playerBottom, computerTop, computerCenter, computerBottom;
    TextView winner;
    Button play;

    int playerCard, computerCard;
    int playerScore=0, computerScore=0, ties=0;

    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //ActionBar actionBar = getSupportActionBar();
        //actionBar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.black)));

        playerTop = findViewById(R.id.playerCardTopLeft);
        playerCenter = findViewById(R.id.playerCardCenter);
        playerBottom = findViewById(R.id.playerCardBottomRight);
        computerTop = findViewById(R.id.computerCardTopLeft);
        computerCenter = findViewById(R.id.computerCardCenter);
        computerBottom = findViewById(R.id.computerCardBottomRight);

        winner = findViewById(R.id.winner);
        play = findViewById(R.id.fire);

        sharedPreferences = getSharedPreferences("MY_SHARED_PREF", MODE_PRIVATE);
        playerScore = sharedPreferences.getInt("playerScore",0);
        computerScore = sharedPreferences.getInt("computerScore",0);
        ties = sharedPreferences.getInt("ties",0);

        Bundle extras = getIntent().getExtras();
        if (extras != null){
            Boolean clicked = extras.getBoolean("isClearClicked");
            if (clicked){
                playerScore = 0;
                computerScore = 0;
                ties = 0;
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        //int id = item.getItemId();

        if (item.getItemId() == R.id.action_score){
            Intent intent = new Intent(this, ScoreActivity.class);
            startActivity(intent);
        }

        else if (item.getItemId() == R.id.howToPlay){
            Intent intent = new Intent(this, HowToPlayActivity.class);
            startActivity(intent);
        }

        //noinspection SimplifiableIfStatement

        return super.onOptionsItemSelected(item);
    }

    public void onPlayClick(View view) {
        playerCard = new Random().nextInt(13) + 2;
        setCards(playerCard,true);

        computerCard = new Random().nextInt(13) + 2;
        setCards(computerCard,false);

        setWinner(playerCard,computerCard);
    }

    public void setWinner(int playerCard, int computerCard){
        SharedPreferences.Editor editor = sharedPreferences.edit();

        if (playerCard > computerCard){
            winner.setText(getResources().getString(R.string.you_win));
            playerScore++;
            editor.putInt("playerScore", playerScore);
        }
        else if (playerCard < computerCard) {
            winner.setText(getResources().getString(R.string.computer_wins));
            computerScore++;
            editor.putInt("computerScore", computerScore);
        }
        else {
            winner.setText("IT'S A TIE");
            ties++;
            editor.putInt("ties",ties);
        }
        editor.commit();
    }

    public void setCards(int random, boolean isPlayerCard){
        String value;
        if (random >= 2 && random <= 10) value = random + "";
        else if (random == 11) value = "J";
        else if (random == 12) value = "Q";
        else if (random == 13) value = "K";
        else  value = "A";

        if (isPlayerCard){
            playerTop.setText(value);
            playerCenter.setText(value);
            playerBottom.setText(value);
        }
        else {
            computerTop.setText(value);
            computerCenter.setText(value);
            computerBottom.setText(value);
        }
    }
}
